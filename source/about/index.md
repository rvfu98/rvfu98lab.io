---
title: About
date: 2018-02-17 19:11:34
---
My name is [苏瑞辅](https://forvo.com/audios/mp3/5/r/5r_9706602_186_5322418.mp3). I always use `rvfu98` as my id.  
I am currently studying aircraft environment and life support engineering in Nanjing University of Aeronautics and Astronautics.
Computer science is my hobby.

## Education Experience
* 2004-2008 苏村南贾小学
* 2008-2010 南宫实验小学
* 2010-2013 [南宫丰翼中学](http://www.ngfyzx.com/index.asp)
* 2013-2016 [河北南宫中学](http://www.hbngzx.cn/index.aspx)
* 2016-Now  [Nanjing University of Aeronautics and Astronautics](http://www.nuaa.edu.cn/)

## Contact With Me
* wechat/cellphone `Kzg2IDE1MTUxODU2Nzgy`
* email `rvfu98#gmail.com`

## External Links
* [Twitter](https://twitter.com/rvfu98)
* [Github](https://github.com/rvfu)
* [微信公众号](http://weixin.sogou.com/weixin?type=2&query=%E8%8B%8F%E7%91%9E%E8%BE%85%E7%9A%84%E7%BD%91%E7%BB%9C%E6%97%A5%E5%BF%97)
